'use strict';

// ready
$(document).ready(function () {

  // custom scroll
  $(".custom-content").mCustomScrollbar({
    axis: "y" // horizontal scrollbar
  });
  // custom scroll

  // mask phone {maskedinput}
  $("[name=phone]").mask("+7 (999) 999-9999");
  // mask phone

  // select {select2}
  $('select').select2({
    minimumResultsForSearch: Infinity
  });
  // select

  //accordion
  $('.toggle--js').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($this.next().hasClass('show')) {
      $this.removeClass('show');
      $this.next().removeClass('show');
      $this.next().slideUp(350);
    } else {
      $this.parent().parent().find('li .inner').removeClass('show');
      $this.parent().parent().find('li .inner').slideUp(350);
      $this.next().toggleClass('show');
      $this.toggleClass('show');
      $this.next().slideToggle(350);
    }
  });
  //accordion

  // popup {magnific-popup}
  $('.popup').magnificPopup({});
  $(document).on('click', '.popup-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });
  // popup

  // input[type=file]
  $('.input__file-js').each(function () {
    $('.input__file-js').change(function () {
      var name = this.value;
      var reWin = /.*\\(.*)/;
      var fileTitle = name.replace(reWin, "$1");
      var reUnix = /.*\/(.*)/;
      fileTitle = fileTitle.replace(reUnix, "$1");
      $(this).parent().find('.btn_js').text("Готово");
      $(this).parent().find('.input__text-js').text(fileTitle);
    });
  });
  // input[type=file]
});
// ready
//# sourceMappingURL=main.js.map
